import React from "react";
import { Redirect } from "react-router-dom";
import { useSelector } from "react-redux";

export const withSessionState = (Component) => (props) => {
  const session = useSelector((state) => state.session);

  if (session) {
    return <Component {...props} />;
  } else {
    return <Redirect to="/login" />;
  }
};
