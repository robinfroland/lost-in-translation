import React from "react";

export const Card = ({ header, content }) => {
  return (
    <div className="card">
      <div className="card-header">{header}</div>
      <div className="card-content">{content}</div>
      <div className="card-footer-accent"></div>
    </div>
  );
};
