import React from "react";
import { signs } from "../utils/imports";
import { useSelector } from "react-redux";

function TranslationOutput() {
  const inputValue = useSelector((state) => state.inputValue);

  const translatedSigns = [...inputValue].map((char, i) => {
    if (char !== " ") {
      return (
        <img src={signs[`./${char.toLowerCase()}.png`]} alt={char} key={i} />
      );
    } else {
      return <span key={i} />;
    }
  });

  return <>{translatedSigns}</>;
}

export default TranslationOutput;
