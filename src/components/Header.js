import React from "react";
import { Link } from "react-router-dom";
import { ReactComponent as ProfileIcon } from "../assets/profile.svg";
import { useSelector, useDispatch } from "react-redux";
import { logoutAction } from "../state/actions/session";
import { clearStore } from "../state/actions/store";

function Header() {
  const username = useSelector((state) => state.session);
  const activeSession = username;
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logoutAction());
    dispatch(clearStore());
  };

  return (
    <header>
      <Link to="/translate">
        <h1 className="logo">Lost in Translation</h1>
      </Link>
      {activeSession && (
        <div>
          {username}
          <div className="dropdown">
            <Link to="/profile">
              <ProfileIcon className="btn-icon" />
            </Link>
            <div className="dropdown-content">
              <Link to="/profile">
                <p>Profile</p>
              </Link>
              <p onClick={() => handleLogout()}>Log out</p>
            </div>
          </div>
        </div>
      )}
    </header>
  );
}

export default Header;
