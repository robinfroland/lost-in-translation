import React from "react";
import TranslationOutput from "../TranslationOutput";
import { withSessionState } from "../../hoc/withSessionState";
import { useDispatch, useSelector } from "react-redux";
import { clearHistory } from "../../state/actions/history";
import { Card } from "../Card";

function Profile() {
  const history = useSelector((state) => state.history);
  const dispatch = useDispatch();

  const displayHistory = history.map((input, i) => {
    return (
      <Card
        header={input}
        content={<TranslationOutput input={input} />}
        key={i}
      />
    );
  });

  const handleClearHistory = () => {
    dispatch(clearHistory());
  };

  return (
    <div className="profile-page">
      <div className="container">
        <h1>Translation History</h1>
        {history.length > 0 ? (
          <button className="btn outline" onClick={handleClearHistory}>
            Clear history
          </button>
        ) : (
          <h2>There are currently no translations in your history</h2>
        )}
        {displayHistory}
      </div>
    </div>
  );
}

export default withSessionState(Profile);
