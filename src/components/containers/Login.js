import React from "react";
import { Redirect } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import { loginAction } from "../../state/actions/session";

import Input from "../Input";
import hello from "../../assets/hello_icon.png";
import { Card } from "../Card";

function Login() {
  const activeSession = useSelector((state) => state.session);
  const dispatch = useDispatch();

  const handleLoginEvent = (username) => {
    dispatch(loginAction(username));
  };

  return (
    <div className="login-wrapper">
      {activeSession && <Redirect to="/translate" />}
      <div className="hero-container">
        <img src={hello} alt="" />
        <h1>Enter your name to get started!</h1>
      </div>
      <div className="container">
        <Card
          content={
            <Input
              context="login"
              onClick={handleLoginEvent}
              placeholder="What's your name?"
            />
          }
        />
      </div>
    </div>
  );
}

export default Login;
