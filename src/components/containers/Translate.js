import React from "react";
import Input from "../Input";
import TranslationOutput from "../TranslationOutput";
import { withSessionState } from "../../hoc/withSessionState";
import { useDispatch, useSelector } from "react-redux";
import { appendHistory } from "../../state/actions/history";
import { setInputValue } from "../../state/actions/translation";
import { Card } from "../Card";

function Translate() {
  const inputValue = useSelector((state) => state.inputValue);
  const username = useSelector((state) => state.session);
  const dispatch = useDispatch();

  const handleTranslation = (input) => {
    const validInput = input.match(/^[a-zA-Z ]+$/) && input.length <= 40;

    if (validInput) {
      dispatch(setInputValue(input));
      dispatch(appendHistory(input));
    } else if (input) {
      alert(
        "\nInvalid translation!\n\nInput can only contain characters from A-Z and must be under 40 characters in length."
      );
    }
  };

  return (
    <div className="translate-page">
      <div className="hero-container">
        <div className="container">
          <h1>Welcome, {username}</h1>
          <Input
            onClick={handleTranslation}
            placeholder="Enter text to translate..."
          />
        </div>
      </div>
      <div className="container">
        <Card
          content={
            inputValue === "" ? (
              <p>Translations will appear here</p>
            ) : (
              <TranslationOutput />
            )
          }
        />
      </div>
    </div>
  );
}

export default withSessionState(Translate);
