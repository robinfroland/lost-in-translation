import React from "react";
import { Link } from "react-router-dom";

function PageNotFound() {
  return (
    <div className="container page-not-found">
      <h1>404</h1>
      <h2>Page not found</h2>
      <p>The page you are looking for doesn't exist!</p>

      <Link to="translate" className="link">
        Click here to go back to safety
      </Link>
    </div>
  );
}

export default PageNotFound;
