import React, { useState } from "react";
import { ReactComponent as IconButton } from "../assets/chevron_right.svg";

function Input({ context = "", placeholder, onClick }) {
  const [state, setState] = useState("");
  const onChangeInput = (e) => setState(e.target.value.trim());
  const onKeyPress = (e) => {
    if (e.keyCode === 13) {
      onClick(state);
    }
  };

  return (
    <div className={`input-container ${context}`}>
      <input
        type="text"
        placeholder={placeholder}
        onChange={onChangeInput}
        onKeyDown={onKeyPress}
      />
      <IconButton className="btn-icon" onClick={() => onClick(state)} />
    </div>
  );
}

export default Input;
