import { createStore, combineReducers } from "redux";
import sessionReducer from "./reducers/session";
import historyReducer from "./reducers/history";
import translationReducer from "./reducers/translation";
import { CLEAR_STORE } from "./actions/store";

const appReducers = combineReducers({
  session: sessionReducer,
  history: historyReducer,
  inputValue: translationReducer,
});

const rootReducer = (state, action) => {
  if (action.type === CLEAR_STORE) {
    return appReducers(undefined, action);
  }
  return appReducers(state, action);
};

export default createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
