export const APPEND_HISTORY = "APPEND_HISTORY";
export const CLEAR_HISTORY = "CLEAR_HISTORY";

export const appendHistory = (input) => ({
  type: APPEND_HISTORY,
  payload: input,
});

export const clearHistory = () => ({
  type: CLEAR_HISTORY,
});
