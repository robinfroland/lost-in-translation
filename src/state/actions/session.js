export const LOGIN = "LOGIN_ACTION";
export const LOGOUT = "LOGOUT_ACTION";

export const loginAction = (username) => ({
  type: LOGIN,
  payload: username,
});

export const logoutAction = () => ({
  type: LOGOUT,
});
