export const SET_INPUT_VALUE = "SET_INPUT_VALUE";

export const setInputValue = (input) => ({
  type: SET_INPUT_VALUE,
  payload: input,
});
