import { SET_INPUT_VALUE } from "../actions/translation";
import { DEFAULT_INPUT_STATE } from "../../utils/constants";

const translationReducer = (state = DEFAULT_INPUT_STATE, action) => {
  switch (action.type) {
    case SET_INPUT_VALUE:
      return action.payload;
    default:
      return state;
  }
};

export default translationReducer;
