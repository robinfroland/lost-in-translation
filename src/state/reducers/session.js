import { DEFAULT_SESSION_STATE, SESSION_KEY } from "../../utils/constants";
import { LOGIN, LOGOUT } from "../actions/session";
import { setStorage, clearStorage } from "../../utils/storage/storage";

const sessionReducer = (state = DEFAULT_SESSION_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      setStorage(SESSION_KEY, action.payload);
      return action.payload;
    case LOGOUT:
      clearStorage();
      return DEFAULT_SESSION_STATE;
    default:
      return state;
  }
};

export default sessionReducer;
