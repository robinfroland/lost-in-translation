import { APPEND_HISTORY, CLEAR_HISTORY } from "../actions/history";
import { HISTORY_KEY } from "../../utils/constants";
import {
  setHistory,
  getHistory,
  DEFAULT_HISTORY_STATE,
} from "../../utils/storage/history";
import { clearStorage } from "../../utils/storage/storage";

const historyReducer = (state = DEFAULT_HISTORY_STATE, action) => {
  switch (action.type) {
    case APPEND_HISTORY:
      setHistory(action.payload);
      return getHistory();
    case CLEAR_HISTORY:
      clearStorage(HISTORY_KEY);
      return DEFAULT_HISTORY_STATE;
    default:
      return state;
  }
};

export default historyReducer;
