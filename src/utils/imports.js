function importSigns(r) {
  let signs = {};
  r.keys().forEach((sign) => {
    signs[sign] = r(sign);
  });
  return signs;
}

export const signs = importSigns(
  require.context("../assets/alphabet", false, /\.(png)$/)
);
