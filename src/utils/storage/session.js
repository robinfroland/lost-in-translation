import { getStorage } from "./storage";
import { SESSION_KEY, DEFAULT_SESSION_STATE } from "../constants";
export const getUserSession = () => {
  const activeSession = getStorage(SESSION_KEY);
  if (!activeSession) {
    return DEFAULT_SESSION_STATE;
  }
  return activeSession;
};
