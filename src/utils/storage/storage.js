export const setStorage = (key, value) => {
  const encryptedVal = btoa(JSON.stringify(value));
  localStorage.setItem(key, encryptedVal);
};

export const getStorage = (key) => {
  let storedVal = localStorage.getItem(key);
  const decryptedVal = atob(storedVal);
  if (storedVal) return JSON.parse(decryptedVal);
  return false;
};

export const clearStorage = (key = false) => {
  key ? localStorage.removeItem(key) : localStorage.clear();
};
