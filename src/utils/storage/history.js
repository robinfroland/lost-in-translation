import { getStorage, setStorage } from "./storage";
import { HISTORY_KEY } from "../constants";

export const DEFAULT_HISTORY_STATE = [];

const initHistory = () => {
  let historyExists = getStorage(HISTORY_KEY);
  if (!historyExists) setStorage(HISTORY_KEY, DEFAULT_HISTORY_STATE);
};

export const setHistory = (value) => {
  initHistory();
  let history = getStorage(HISTORY_KEY);
  if (history.length === 10) history.pop();
  setStorage(HISTORY_KEY, [value, ...history]);
};

export const getHistory = () => {
  initHistory();
  return getStorage(HISTORY_KEY);
};
