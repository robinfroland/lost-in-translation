export const HISTORY_KEY = "translation-history";
export const SESSION_KEY = "user-session";

export const DEFAULT_SESSION_STATE = "";
export const DEFAULT_INPUT_STATE = "";
