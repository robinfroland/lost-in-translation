import React from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Translate from "./components/containers/Translate";
import Login from "./components/containers/Login";
import Profile from "./components/containers/Profile";
import Header from "./components/Header";

import PageNotFound from "./components/containers/PageNotFound";
import { useDispatch } from "react-redux";
import { getUserSession } from "./utils/storage/session";
import { loginAction } from "./state/actions/session";

function App() {
  const dispatch = useDispatch();
  const activeSession = getUserSession();

  if (activeSession) {
    // Dispatch session to persist on reload
    dispatch(loginAction(activeSession));
  }

  return (
    <Router>
      <Header />
      <Switch>
        <Redirect exact from="/" to="/login" />
        <Route path="/translate" component={Translate} />
        <Route path="/login" component={Login} />
        <Route path="/profile" component={Profile} />
        <Route path="*" component={PageNotFound} />
      </Switch>
    </Router>
  );
}

export default App;
